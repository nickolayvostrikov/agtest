#Copyright (C) 2014-2017 Nickolai  Vostrikov
#
#Licensed to the Apache Software Foundation( ASF ) under one
#or more contributor license agreements.See the NOTICE file
#distributed with this work for additional information
#regarding copyright ownership.The ASF licenses this file
#to you under the Apache License, Version 2.0 ( the
#	"License" ); you may not use this file except in compliance
#	with the License.You may obtain a copy of the License at
#
#	http ://www.apache.org/licenses/LICENSE-2.0
#	or read LICENSE file in root repository directory
#
#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an
#"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
#KIND, either express or implied.See the License for the
#specific language governing permissions and limitations
#under the License.


set( PV_VERSION_MAJOR 0 )
set( PV_VERSION_MINOR 1 )
set( PV_VERSION_PATCH 0 )
set( PV_VERSION_DEVTWEAK 1 )

set( PV_VERSION ${PV_VERSION_MAJOR}.${PV_VERSION_MINOR}.${PV_VERSION_PATCH}.${PV_VERSION_DEVTWEAK} )
set( PV_VERSION_STRING "${PV_VERSION_MAJOR}.${PV_VERSION_MINOR}.${PV_VERSION_PATCH}.${PV_VERSION_DEVTWEAK}")


#option( BUILD_SHARED_LIBS "Build shared libraries. Default TRUE" ON )

if( BUILD_SHARED_LIBS )
	set( PV_SHARED_LIBS TRUE )
else( )
	set( PV_SHARED_LIBS FALSE )
endif( )

#message( STATUS "${PV_SHARED_LIBS}" )

set(CMAKE_DEBUG_POSTFIX "d")

set( PV_OUTPUT_DIR "${PROJECT_BINARY_DIR}" )

set( PV_OUTPUT_DIR_RUNTIME "${PV_OUTPUT_DIR}/bin" )
set( PV_OUTPUT_DIR_EXE "${PV_OUTPUT_DIR}/bin" )
set( PV_OUTPUT_DIR_ARH "${PV_OUTPUT_DIR}/lib" )

file( MAKE_DIRECTORY ${PV_OUTPUT_DIR_RUNTIME} )
file( MAKE_DIRECTORY ${PV_OUTPUT_DIR_EXE} )
file( MAKE_DIRECTORY ${PV_OUTPUT_DIR_ARH} )

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PV_OUTPUT_DIR_RUNTIME}" )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PV_OUTPUT_DIR_RUNTIME}" )
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY  "${PV_OUTPUT_DIR_ARH}" )
set( EXECUTABLE_OUTPUT_PATH "${PV_OUTPUT_DIR_EXE}" )

set( PV_PLATFORM_WINDOWS 0 )
set( PV_PLATFORM_LINUX 1 )
set( PV_PLATFORM_MACOS 2 )
set( PV_PLATFORM_DARWIN 3 )
set( PV_PLATFORM_ANDROID 4 )

set( PV_COMPILER_MSVC 0 )
set( PV_COMPILER_CLANG 1)
set( PV_COMPILER_GCC 2 ) 
set( PV_PTRSIZE 0 )

#if( WIN32 OR WIN64 )
#	set( PV_PLATFORM "${PV_PLATFORM_WINDOWS}" )
#elseif( LINUX32 OR LINUX64 )
#	set( PV_PLATFORM "${PV_PLATFORM_LINUX}" )
#else( )
#	message( FATAL_ERROR "Unknown platform (Operation System)" )
#endif( )


if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" )
	set( PV_COMPILER "${PV_COMPILER_CLANG}" )
elseif( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" )
	set( PV_COMPILER "${PV_COMPILER_GCC}" )
elseif( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" )
	set( PV_COMPILER "${PV_COMPILER_MSVC}" )
else( )
  message( FATAL_ERROR "Unknown (currently not supported) compiler" )
endif()

set( PV_PTRSIZE "${CMAKE_SIZEOF_VOID_P}" )



set( PV_BUILD_CONFIGURATIONS Release RelWithDebInfo Debug )
set( DOC_STRING "Specify CMake build configuration (single-configuration generator only), possible values are Release (default), RelWithDebInfo, and Debug")
if( CMAKE_CONFIGURATION_TYPES )
    # For multi-configurations generator, such as VS and Xcode
    set( CMAKE_CONFIGURATION_TYPES ${PV_BUILD_CONFIGURATIONS} CACHE STRING ${DOC_STRING} FORCE )
    unset( CMAKE_BUILD_TYPE )
else( )
    # For single-configuration generator, such as Unix Makefile generator
    if( CMAKE_BUILD_TYPE STREQUAL "" )
        # If not specified then default to Release
        set(CMAKE_BUILD_TYPE Release )
    endif( )
    set( CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING ${DOC_STRING} FORCE )
endif ( )


option( PV_ENABLE_DOXYFILE "Generate documentation as part of normal build")
option( PPV_ENABLE_LOGGING "Enable logging support" FALSE )
option( PV_ENABLE_PROFILING "Enable profiling support (only for Release builds)" FALSE )
option( PV_USE_PCH "Enable Precompiled Headers" FALSE )

if( PV_ENABLE_PROFILING )
    add_definitions( -DPV_PROFILING )
endif( )

if( PPV_ENABLE_LOGGING )
	add_definitions( -DPV_LOGGING )
endif( )

if( PV_ENABLE_DOXYFILE )
	find_package( FindDoxygen )
	if( DOXYGEN_FOUND )
		add_definitions( -DPV_PROFILING )
	else( )
		message( STATUS "Documentation generation enabled but doxygen not foundю Skipped!" )
		set( PV_GENDOCS FALSE )
	endif( )
endif( )


function (disallow_intree_builds)
  # Adapted from LLVM's toplevel CMakeLists.txt file
  if( CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR )
    message(FATAL_ERROR "
      In-source builds are not allowed. CMake would overwrite the
      makefiles distributed with utf8proc. Please create a directory
      and run cmake from there. Building in a subdirectory is
      fine, e.g.:
      
        mkdir build
        cd build
        cmake ..
      
      This process created the file `CMakeCache.txt' and the
      directory `CMakeFiles'. Please delete them.
      
      ")
  endif()
endfunction()


macro ( mark_as_internal _var )
  set ( ${_var} ${${_var}} CACHE INTERNAL "hide this!" FORCE )
endmacro( mark_as_internal _var )















