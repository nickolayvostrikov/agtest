#include "Framework.hxx"
#include "AGTPlayCtrl.hxx"
#include "MainScene.hxx"


const sf::String CONFIG_FILE_NAME = "config.cfg";
const sf::String MAIN_SCENE_NAME = "MainScene";

int main( int argv, char* arvc[] )
{

	Utils::initWorkingDirFromArv0( arvc[0] );
	AGTPlayerController plctrl;
	Application app( Utils::workingDir( ) + CONFIG_FILE_NAME, &plctrl );

	std::unique_ptr<MainScene> pMainScene = Utils::make_unique<MainScene>(  );

	app.addScene( &*pMainScene, MAIN_SCENE_NAME, true );

	return app.run( );
}