#pragma once
#include "MainSceneEmitter.hxx"


class StarEmitter final: public MainSceneEmitter
{
private:
	sf::Vector2f                 mPosition;
	Distribution<sf::Vector2f>   mVelocity;
	sf::Time                     mLifeTime;
public:
	StarEmitter( int32_t id,
		unsigned int texId );
	~StarEmitter( );

	virtual void init( const ParticleEvent& pe ) override;

	virtual void operator( ) ( EmissionInterface& system, sf::Time dt ) override;

};





