#include "Framework.hxx"
#include "BallEmitter.hxx"


const float BALL_VELOCITY_ROT_ANGLE = 20.0f;
const float BALL_VELOCITY_NOS_AMOUNT = 2.0f;

BallEmitter::BallEmitter( int32_t id,
	unsigned int texId ):
	MainSceneEmitter( id, texId )
	,mPosition( sf::Vector2f( 0.0f, 0.0f ) )
	,mVelocity( sf::Vector2f( 0.0f, 0.0f ) )
{
	
}

BallEmitter::~BallEmitter( )
{

}

void BallEmitter::init( const ParticleEvent& pe )
{
	/*Store die gun particle position as
	start point for emitting new ball particles
	*/
	mPosition = pe.mPosition;
	
	/*
	Total life time of each ball particle is
	about 1/2 of total life time of "parent" gun particle
	*/
	sf::Time tempT = sf::seconds( pe.mTotalLifetime.asSeconds( ) / 2.0f );
	mLifeTime = sf::seconds( random( tempT.asSeconds( ) - BALL_STAR_LIFITIME_MODIFIER,
		tempT.asSeconds( ) + BALL_STAR_LIFITIME_MODIFIER ) );

	/*
	Velocity of new ball particle is gun velocity * 2
	and start vector is small different of "parent"
	So it NOS injection and small rotation
	*/
	mVelocity = deflect( ( pe.mVelocity * BALL_VELOCITY_NOS_AMOUNT ), BALL_VELOCITY_ROT_ANGLE );
}

void BallEmitter::operator( ) ( EmissionInterface& system, sf::Time dt )
{
	//Emit 2 balls
	for( int i = 0; i < 2; ++i )
	{
		Particle pt( mLifeTime );
		pt.mParentEmitter = this;
		pt.mPosition = mPosition;
		pt.mVelocity = mVelocity( );
		pt.mTextureIndex = mParticleTextureIndex( );

		system.emitParticle( pt );
	}
}

