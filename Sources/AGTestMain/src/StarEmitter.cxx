#include "Framework.hxx"
#include "StarEmitter.hxx"


StarEmitter::StarEmitter( int32_t id,
	unsigned int texId ):
	MainSceneEmitter( id, texId )
	, mPosition( sf::Vector2f( 0.0f, 0.0f ) )
	, mVelocity( sf::Vector2f( 0.0f, 0.0f ) )
{

}

StarEmitter::~StarEmitter( )
{

}

void StarEmitter::init( const ParticleEvent& pe )
{
	/*Store die gun or ball particle position as
	  start point for emitting new stars particles
	*/
	mPosition = pe.mPosition;

	/*
	Total life time of each star particle is
	about 1/2 of total life time of "parent" gun\ball particle
	*/
	sf::Time tempT = sf::seconds( pe.mTotalLifetime.asSeconds( ) / 2.0f );
	mLifeTime = sf::seconds( random( tempT.asSeconds( ) - BALL_STAR_LIFITIME_MODIFIER,
		tempT.asSeconds( ) + BALL_STAR_LIFITIME_MODIFIER ) );

	/*
	Velocity of new star particle is direct ratio of "parent" life time
	and start vector is random
	So it NOS injection 
	*/
	auto velMod = pe.mTotalLifetime.asSeconds( ) / 2.0f;
	if( velMod < 1.0f )
		velMod = 1.0f;
	mVelocity = pe.mVelocity * velMod;
}

void StarEmitter::operator( ) ( EmissionInterface& system, sf::Time dt )
{
	auto starsCount = random( FApp( )->configFile( )->intValueOf( "StarsMinCount", 20 ),
		FApp( )->configFile( )->intValueOf( "StarsMaxCount", 40 ) );
	
	for( auto i = 0; i < starsCount; ++i )
	{
		Particle pt( mLifeTime );
		pt.mParentEmitter = this;
		pt.mPosition = mPosition;
		pt.mVelocity = deflect( mVelocity( ), 360.0 )( );
		pt.mTextureIndex = mParticleTextureIndex( );

		system.emitParticle( pt );
	}
}


