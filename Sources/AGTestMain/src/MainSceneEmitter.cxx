#include "Framework.hxx"
#include "MainSceneEmitter.hxx"

MainSceneEmitter::MainSceneEmitter( int32_t id,
	unsigned int texId ):
	BaseEmitter( id, texId )
	, mPosition( sf::Vector2f( 0.0f, 0.0f ) )
	, mVelocity( sf::Vector2f( 0.0f, 0.0f ) )
	, mLifeTime(  )
{

}

MainSceneEmitter::~MainSceneEmitter( )
{

}

