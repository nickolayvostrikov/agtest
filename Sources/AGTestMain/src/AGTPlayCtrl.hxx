#pragma once

/* 
    This very powerful class filled with spirit of Unreal Engine 4!
	UE4 is best graphics engine and editor on the world! 
	:)))
*/
class AGTPlayerController : public IPlayerController
{
public:
	AGTPlayerController( );
	~AGTPlayerController( );

	virtual void onKeypressed( sf::Keyboard::Key& k ) override;
};