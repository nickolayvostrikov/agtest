#pragma once
 
#include "BallEmitter.hxx"
#include "StarEmitter.hxx"

enum EmittersTypeID: int32_t
{
	GunEmitterID = 1,
	BallEmitterID,
	StarEmitterID
};

typedef BallEmitter* BallEmitterPtr;
typedef std::unordered_map<BallEmitterPtr, bool> BallEmitterMap;

typedef StarEmitter* StarEmitterPtr;
typedef std::unordered_map<StarEmitterPtr, bool> StarEmitterMap;

class MainScene final : public IRenderScene
{
private:
	ParticleSystem     mPSEngine;
	UniversalEmitter   mGunEmitter;
	BallEmitterMap     mBallEmitterMap;
	StarEmitterMap     mStarEmitterMap;
	sf::Texture        mPSAtlasTexture;
	unsigned int       mGunTexID;
	unsigned int       mBallTexID;
	unsigned int       mStarTexID;

	void onParticleDie( const ParticleEvent& pe );

	void onGunParticleDie( const ParticleEvent& pe );

	void onBallParticleDie( const ParticleEvent& pe );

	void onStarParticleDie( const ParticleEvent& pe );

	void addNewEmitter( const ParticleEvent& pe, EmittersTypeID emiType );
	
public:
	MainScene( );
	~MainScene( );

	virtual void onLoading( ) override;

	virtual void onUnloading( ) override;

	virtual void onResize( unsigned int newWidth, unsigned int newHeight ) override;

	virtual void onSetCurrent( ) override;

	virtual void onLoseCurrent( ) override;

	virtual void prepareFrame( sf::Time deltaTime ) override;

	virtual void renderFrame( sf::RenderWindow& rt ) override;

	virtual void endFrame( ) override;
};