#pragma once

static const float BALL_STAR_LIFITIME_MODIFIER = 0.2f;

/*
	Base class for main scene emitters.
	Used as facade interface for Ball and Star emitters
*/

class MainSceneEmitter: public BaseEmitter
{
protected:
	sf::Vector2f                 mPosition;
	Distribution<sf::Vector2f>   mVelocity;
	sf::Time                     mLifeTime;
public:
	MainSceneEmitter( int32_t id,
		unsigned int texId );
	virtual ~MainSceneEmitter( );

	virtual void init( const ParticleEvent& pe ) = 0;
};

