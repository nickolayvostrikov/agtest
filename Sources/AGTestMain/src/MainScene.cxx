#include "Framework.hxx"
#include "BallEmitter.hxx"
#include "StarEmitter.hxx"
#include "MainScene.hxx"


const float UP_ANGLE = -90.0f;
const sf::Time EXPLOSION_DURATION = sf::seconds( 0.2f );

const sf::String PS_ATLAS_TEXTURE_NAME = "res/ps_atlas.png";
const sf::IntRect PS_GUN_TEXTURE_RC = sf::IntRect( 0, 0, 16, 16 );
const sf::IntRect PS_BALL_TEXTURE_RC = sf::IntRect( 16, 0, 16, 16 );
const sf::IntRect PS_STAR_TEXTURE_RC = sf::IntRect( 16+16, 0, 8, 8 );

MainScene::MainScene( ) :
	 mGunEmitter( GunEmitterID, 0u )
{
	//You like std::function ? 
	//I too highly :)
	mPSEngine.addParticleEventListener( ParticleEventType::Die,
		std::bind( &MainScene::onParticleDie, this, std::placeholders::_1 ) );
	
}

MainScene::~MainScene( )
{

}

void MainScene::onLoading( )
{
	auto gt = mPSAtlasTexture.loadFromFile( Utils::workingDir() + PS_ATLAS_TEXTURE_NAME );
	assert( gt );
	
	mPSEngine.setTexture( mPSAtlasTexture );

	mGunTexID = mPSEngine.addTextureRect( PS_GUN_TEXTURE_RC );
	mBallTexID = mPSEngine.addTextureRect( PS_BALL_TEXTURE_RC );
	mStarTexID = mPSEngine.addTextureRect( PS_STAR_TEXTURE_RC );
	
	mGunEmitter.setEmitterID( GunEmitterID );

	mGunEmitter.setEmissionRate( 
		FApp( )->configFile( )->floatValueOf( "GunEmissionRate", 0.5f ) );
	mGunEmitter.setParticleLifetime(
		FApp( )->configFile( )->floatValueOf( "GunMinParticleLifetime", 2.0f ),
		FApp( )->configFile( )->floatValueOf( "GunMaxParticleLifetime", 3.0f ) 
	);
	mGunEmitter.setParticleTextureIndex( mGunTexID );
	
	mPSEngine.addEmitter( refEmitter( mGunEmitter) );
	mPSEngine.addEffectModifier( ForceEffect( sf::Vector2f( 0.f, 
		FApp( )->configFile( )->floatValueOf( "GravityForce", 100.0f) ) ) );
	mPSEngine.addEffectModifier( TorqueEffect(  FApp( )->configFile( )->floatValueOf( 
		"TorqueForce", 100.0f ) ) );

	auto gunVelocity = PolarVector2f( 150.0f, UP_ANGLE );

	auto emitConeAngle = FApp( )->configFile( )->floatValueOf( "GunEmitConeAngle", 45.0f );

	mGunEmitter.setParticleVelocity( deflect( gunVelocity, emitConeAngle ) );
	mGunEmitter.setParticleVelocityModifier( FApp( )->configFile( )->floatValueOf( "GunParticleVelocityMod", 1.5f ) );
	
	/*
		At this point we can simple calculate max ball and star particle emitters count
		for reduce amount of memory allocations on render time (via new *Emitter(...) )
	*/
	auto gunEmiRate = FApp( )->configFile( )->floatValueOf( "GunEmissionRate", 0.5f );
	auto gunEmiMaxLife = FApp( )->configFile( )->floatValueOf( "GunMaxParticleLifetime", 3.0 );

	size_t gunsCount = static_cast<size_t>( std::ceil( gunEmiMaxLife / gunEmiRate ) );
	/*
	 Ball emitters at twice as much of gun emitters count
	*/
	for( auto b = 0; b < gunsCount * 2; ++b )
	{
		BallEmitterPtr newBallEmi = new BallEmitter( BallEmitterID, mBallTexID );
		mBallEmitterMap[newBallEmi] = true;
	}

	/*
	 Star emitters as well as ball emitters plus guns emitters
	*/
	for( auto s = 0; s < ( gunsCount + ( gunsCount * 2 ) ); ++s );
	{
		StarEmitterPtr newStarEmi = new StarEmitter( StarEmitterID, mStarTexID );
		mStarEmitterMap[newStarEmi] = true;
	}
}

void MainScene::onUnloading( )
{
	for( auto& ballemis : mBallEmitterMap )
		delete ballemis.first;
	
	mBallEmitterMap.clear( );
	for( auto& staremis : mStarEmitterMap )
		delete staremis.first;

	mStarEmitterMap.clear( );
}

void MainScene::onResize( unsigned int newWidth, unsigned int newHeight )
{
	IRenderScene::onResize( newWidth, newHeight );
	auto psPos = sf::Vector2f( float( mWidth / 2 ),
		float( mHeight - Utils::percentsOf( mHeight, 15u ) ) );
	mGunEmitter.setParticlePosition( psPos );
}

void MainScene::onSetCurrent( )
{

}

void MainScene::onLoseCurrent( )
{

}

void MainScene::prepareFrame( sf::Time deltaTime )
{
	mPSEngine.update( deltaTime );
}

void MainScene::renderFrame( sf::RenderWindow& rt )
{
	auto psPos = sf::Vector2f( float( rt.getSize().x / 2 ),
		float( rt.getSize( ).y - Utils::percentsOf( rt.getSize( ).y, 15u ) ) );
	mGunEmitter.setParticlePosition( psPos );
	rt.draw( mPSEngine );
}

void MainScene::endFrame( )
{

}


void MainScene::onParticleDie( const ParticleEvent& pe )
{
	/*After life time of particle  is end we catch this
	  event. We have id from it emitter and other useful data  
	*/
	auto emiD = pe.mParentEmitter->emitterID( );
	switch( emiD )
	{
	case GunEmitterID:
		onGunParticleDie( pe );
		break;
	case BallEmitterID:
		onBallParticleDie( pe );
		break;
	case StarEmitterID:
		onStarParticleDie( pe );
	default:
		return;
	}
}

void MainScene::onGunParticleDie( const ParticleEvent& pe )
{
	
	addNewEmitter( pe, BallEmitterID );
	addNewEmitter( pe, StarEmitterID );
}

void MainScene::onBallParticleDie( const ParticleEvent& pe )
{
	auto itshot = mBallEmitterMap.find( (BallEmitterPtr)pe.mParentEmitter );
	if( itshot != mBallEmitterMap.end( ) )
	{
		//Mark this emitter as "after shot" for reuse.
		itshot->second = false;
	}
	addNewEmitter( pe, StarEmitterID );
}

void MainScene::onStarParticleDie( const ParticleEvent& pe )
{
	auto itshot = mStarEmitterMap.find( (StarEmitterPtr)pe.mParentEmitter );
	if( itshot != mStarEmitterMap.end( ) )
	{
		//Mark this emitter as "after shot" for reuse.
		itshot->second = false;
	}
}

void MainScene::addNewEmitter( const ParticleEvent& pe, EmittersTypeID emiID )
{
	MainSceneEmitter*  newEmitter = nullptr;
	//First we find "free" emitter (emitter after single shot )
	if( emiID == BallEmitterID )
	{
		for( auto& item : mBallEmitterMap )
		{
			if( item.second == false )
			{
				newEmitter = item.first;
			}
		}
	}
	else
	{
		for( auto& item : mStarEmitterMap )
		{
			if( item.second == false )
			{
				newEmitter = item.first;
			}
		}
	}

	if( !newEmitter )
	{
		//Need new emitter
		if( emiID == BallEmitterID )
		{
		    newEmitter = new BallEmitter( BallEmitterID, mBallTexID );
		}
		else
		{
			newEmitter = new StarEmitter( StarEmitterID, mStarTexID );
		}
	}
	/*Setup emitter for new shot and add to map
	with mark as "live"
	*/
	newEmitter->init( pe );
	mPSEngine.addEmitter( refEmitter( *newEmitter ), true );
	if( emiID == BallEmitterID )
	{
		mBallEmitterMap[(BallEmitterPtr)newEmitter] = true;
	}
	else
	{
		mStarEmitterMap[(StarEmitterPtr)newEmitter] = true;
	}
}


