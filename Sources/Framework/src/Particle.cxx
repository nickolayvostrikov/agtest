#include "Common.hxx"
#include "Particle.hxx"

Particle::Particle( sf::Time timeTotalLife )
	: mPosition( )
	, mVelocity( )
	, mRotation( )
	, mRotationSpeed( )
	, mScale( 1.f, 1.f )
	, mColor( 255, 255, 255 )
	, mTextureIndex( 0 )
	, mPassedLifetime( sf::Time::Zero )
	, mTotalLifetime( timeTotalLife )
{
}

sf::Time elapsedLifetime( const Particle& particle )
{
	return particle.mPassedLifetime;
}

sf::Time totalLifetime( const Particle& particle )
{
	return particle.mTotalLifetime;
}

sf::Time remainingLifetime( const Particle& particle )
{
	return totalLifetime( particle ) - elapsedLifetime( particle );
}

float elapsedRatio( const Particle& particle )
{
	return elapsedLifetime( particle ) / totalLifetime( particle );
}

float remainingRatio( const Particle& particle )
{
	return remainingLifetime( particle ) / totalLifetime( particle );
}

void abandonParticle( Particle& particle )
{
	particle.mPassedLifetime = particle.mTotalLifetime;
}
