#include "Common.hxx"
#include "Distrib.hxx"
#include "PolarVector2.hxx"
#include "VectorUtils.hxx"
#include "Particle.hxx"
#include "Emitter.hxx"
#include "UniversalEmitter.hxx"
#include "Random.hxx"


UniversalEmitter::UniversalEmitter( int32_t id, unsigned int texId )
	: BaseEmitter( id, texId )
	, mEmissionRate( 1.f )
	, mEmissionDifference( 0.f )
	, mParticleLifeTimeMin( 1.f )
	, mParticleLifeTimeMax( 2.f )
	, mParticlePosition( sf::Vector2f( 0.f, 0.f ) )
	, mParticleVelocity( sf::Vector2f( 0.f, 0.f ) )
	, mParticleVelocityModifier( 1.0f )
	, mParticleRotation( 0.f )
	, mParticleRotationSpeed( 0.f )
	, mParticleScale( sf::Vector2f( 1.f, 1.f ) )
	, mParticleColor( sf::Color::White )
{
	
}

UniversalEmitter::~UniversalEmitter( )
{

}

void UniversalEmitter::operator() ( EmissionInterface& system, sf::Time dt )
{
	const std::size_t nbParticles = computeParticleCount( dt );

	for( std::size_t i = 0; i < nbParticles; ++i )
	{
		// Create particle and specify parameters
	    sf::Time ttl = sf::seconds( random( mParticleLifeTimeMin( ), mParticleLifeTimeMax( ) ) );
		sf::Vector2f vel = mParticleVelocity( ) * random( 1.0, mParticleVelocityModifier( ) );
		Particle particle( ttl  );
		particle.mPosition = mParticlePosition( );
		particle.mVelocity = vel;
		particle.mRotation = mParticleRotation( );
		particle.mRotationSpeed = mParticleRotationSpeed( );
		particle.mScale = mParticleScale( );
		particle.mColor = mParticleColor( );
		particle.mTextureIndex = mParticleTextureIndex( );
		particle.mParentEmitter = this;

		system.emitParticle( particle );
	}
}

void UniversalEmitter::setEmissionRate( float particlesPerSecond )
{
	mEmissionRate = particlesPerSecond;
}

void UniversalEmitter::setParticleLifetime( Distribution<float> prtLifetimeMin, Distribution<float> prtLifetimeMax )
{
	mParticleLifeTimeMin = std::move( prtLifetimeMin );
	mParticleLifeTimeMax = std::move( prtLifetimeMax );
}

void UniversalEmitter::setParticlePosition( Distribution<sf::Vector2f> particlePosition )
{
	mParticlePosition = std::move( particlePosition );
}

void UniversalEmitter::setParticleVelocity( Distribution<sf::Vector2f> particleVelocity )
{
	mParticleVelocity= std::move( particleVelocity );
}

void UniversalEmitter::setParticleVelocityModifier( Distribution<float> particleVelMod )
{
	assert( particleVelMod( ) >= 0.0f );
	mParticleVelocityModifier = std::move( particleVelMod );
}

void UniversalEmitter::setParticleRotation( Distribution<float> particleRotation )
{
	mParticleRotation = std::move( particleRotation );
}

void UniversalEmitter::setParticleRotationSpeed( Distribution<float> particleRotationSpeed )
{
	mParticleRotationSpeed = std::move( particleRotationSpeed );
}

void UniversalEmitter::setParticleScale( Distribution<sf::Vector2f> particleScale )
{
	mParticleScale = std::move( particleScale );
}

void UniversalEmitter::setParticleColor( Distribution<sf::Color> particleColor )
{
	mParticleColor = std::move( particleColor );
}

std::size_t UniversalEmitter::computeParticleCount( sf::Time dt )
{
	// We want to fulfill the desired particle rate as exact as possible. Since the amount of emitted particles per frame is
	// integral, we have to emit sometimes more and sometimes less. mParticleDifference takes care of the deviation each frame.
	float particleAmount = mEmissionRate * dt.asSeconds( ) + mEmissionDifference;
	std::size_t nbParticles = static_cast< std::size_t >( particleAmount );

	// Compute difference for next frame, return current amount
	mEmissionDifference = particleAmount - nbParticles;
	return nbParticles;
}
