#include "Common.hxx"
#include "Utils.hxx"

namespace Utils
{
	static std::string gStrWorkingDir;

	void initWorkingDirFromArv0( const char* argv0 )
	{
		std::string tmp( argv0 );
		size_t posNIX = tmp.find_last_of( '/' );
		size_t posNT = tmp.find_last_of( '\\' );

		size_t pos = 0;
		if( posNIX != std::string::npos )
		{
			pos = posNIX;
		}
		if( posNT != std::string::npos )
		{
			pos = posNT;
		}
		gStrWorkingDir = tmp.substr( 0, pos+1 );
	}

	std::string workingDir( )
	{
		return gStrWorkingDir;
	}
}