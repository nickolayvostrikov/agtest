#include "Common.hxx"
#include "Application.hxx"
#include "AppPrivate.hxx"
#include "IPlayerController.hxx"

Application::Application( const sf::String& strConfigFileName, IPlayerController* pPlayCtrl )
{
	if( gpAppInstance )
	{
		assert( !"Only one instance of Application can be created!" );
		printf( "Only one instance of Application can be created!" );
		exit( -1 );
		return;
	}
	assert( pPlayCtrl );
	mpImpl = Utils::make_unique<AppPrivate>( strConfigFileName, pPlayCtrl );
	gpAppInstance = this;
}

Application::~Application( )
{
	gpAppInstance = nullptr;
}

Application* Application::instance( )
{
	return gpAppInstance;
}

void Application::addScene( IRenderScene* newScene, const sf::String& strSceneName, bool bSetAsCurrent /*= false*/ )
{
	return mpImpl->addScene( newScene, strSceneName, bSetAsCurrent );
}

int Application::run( )
{
	return mpImpl->run( );
}

void Application::quit( )
{
	return mpImpl->quit( );
}

const ConfigFile* Application::configFile( )
{
	return &mpImpl->mCfgFile;
}
