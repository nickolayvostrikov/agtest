#include "Common.hxx"
#include "Connection.hxx"
#include "ConnectionImpl.hxx"
#include "Distrib.hxx"
#include "UniversalEmitter.hxx"
#include "Particle.hxx"
#include "ParticleSystem.hxx"
#include <SFML/Graphics/Vertex.hpp>

// Erases emitter/EffectModifieror at itr from ctr, if its time has expired. itr will point to the next element.
template <class Container> void incrementCheckExpiry( Container& ctr, typename Container::iterator& itr, sf::Time dt )
{
	// itr->second is the remaining time of the emitter/EffectModifieror.
	// Time::Zero means infinite time (no removal).
	if( itr->timeUntilRemoval != sf::Time::Zero && ( itr->timeUntilRemoval -= dt ) <= sf::Time::Zero  )
	{
		itr = ctr.erase( itr );
	}
	else
	{
		++itr;
	}
}

template <class Container> void checkForSingleShot( Container& ctr, typename Container::iterator& itr )
{
	if( itr->singleshot && !itr->beforeshot )
	{
		itr = ctr.erase( itr );
	}
	else
	{
		++itr;
	}
}

sf::IntRect fullRect( const sf::Texture& texture )
{
	return sf::IntRect( 0, 0, texture.getSize( ).x, texture.getSize( ).y );
}

ParticleSystem::ParticleSystem( )
	: mParticles( )
	, mEffectModifiers( )
	, mEmitters( )
	, mTexture( nullptr )
	, mTextureRects( )
	, mVertices( sf::Quads )
	, mNeedsVertexUpdate( true )
	, mQuads( )
	, mNeedsQuadUpdate( true )
{
}

ParticleSystem::ParticleSystem( const ParticleSystem&& source ) noexcept
	: mParticles( std::move( source.mParticles ) )
	, mEffectModifiers( std::move( source.mEffectModifiers ) )
	, mEmitters( std::move( source.mEmitters ) )
	, mTexture( std::move( source.mTexture ) )
	, mTextureRects( std::move( source.mTextureRects ) )
	, mVertices( std::move( source.mVertices ) )
	, mNeedsVertexUpdate( std::move( source.mNeedsVertexUpdate ) )
	, mQuads( std::move( source.mQuads ) )
	, mNeedsQuadUpdate( std::move( source.mNeedsQuadUpdate ) )
	, mParticleEventSys( std::move( source.mParticleEventSys ) )
{
}

ParticleSystem& ParticleSystem::operator = ( const ParticleSystem&& source ) noexcept
{
	mParticles = std::move( source.mParticles );
	mEffectModifiers = std::move( source.mEffectModifiers );
	mEmitters = std::move( source.mEmitters );
	mTexture = std::move( source.mTexture );
	mTextureRects = std::move( source.mTextureRects );
	mVertices = std::move( source.mVertices );
	mNeedsVertexUpdate = std::move( source.mNeedsVertexUpdate );
	mQuads = std::move( source.mQuads );
	mNeedsQuadUpdate = std::move( source.mNeedsQuadUpdate );
	mParticleEventSys = std::move( source.mParticleEventSys );
	return *this;
}

void ParticleSystem::setTexture( const sf::Texture& texture )
{
	mTexture = &texture;
	mNeedsQuadUpdate = true;
}

unsigned int ParticleSystem::addTextureRect( const sf::IntRect& textureRect )
{
	mTextureRects.push_back( textureRect );
	mNeedsQuadUpdate = true;

	return static_cast< unsigned int >( mTextureRects.size( ) - 1 );
}

Connection ParticleSystem::addEffectModifier( std::function<void( Particle&, bool, sf::Time )> effectMod )
{
	return addEffectModifier( std::move( effectMod ), sf::Time::Zero );
}

Connection ParticleSystem::addEffectModifier( std::function<void( Particle&, bool,  sf::Time )>effectMod, sf::Time timeUntilRemoval )
{
	mEffectModifiers.push_back( EffectModifPred( std::move( effectMod ), false, timeUntilRemoval ) );
	mEffectModifiers.back( ).tracker = makeIdConnectionImpl( mEffectModifiers, mEffectModifiers.back( ).id );

	return Connection( mEffectModifiers.back( ).tracker );
}


void ParticleSystem::clearEffectModifiers( )
{
	mEffectModifiers.clear( );
}

Connection ParticleSystem::addEmitter( std::function<void( EmissionInterface&, bool, sf::Time )> emitter ) 
{
	return addEmitter( emitter, false, sf::Time::Zero );
}

Connection ParticleSystem::addEmitter( std::function<void( EmissionInterface&, bool, sf::Time )> emitter, bool singleshot )
{
	return addEmitter( emitter, singleshot, sf::Time::Zero );
}

Connection ParticleSystem::addEmitter( std::function<void( EmissionInterface&, bool, sf::Time )> emitter, bool singleshot, sf::Time timeUntilRemoval )
{
	mEmitters.push_back( Emitter( std::move( emitter ), singleshot, timeUntilRemoval ) );
	mEmitters.back( ).tracker = makeIdConnectionImpl( mEmitters, mEmitters.back( ).id );

	return Connection( mEmitters.back( ).tracker );
}

void ParticleSystem::clearEmitters( )
{
	mEmitters.clear( );
}

void ParticleSystem::update( sf::Time dt )
{
	// Invalidate stored vertices
	mNeedsVertexUpdate = true;

	// Emit new particles and remove expiring emitters
	for( EmitterContainer::iterator itr = mEmitters.begin( ); itr != mEmitters.end( ); )
	{
		itr->function( *this, true, dt );
		itr->beforeshot = false;
		incrementCheckExpiry( mEmitters, itr, dt );
	}
	//Remove single shot emitters
	for( EmitterContainer::iterator itr = mEmitters.begin( ); itr != mEmitters.end( ); )
	{
		checkForSingleShot( mEmitters, itr );
	}

	// EffectModifier existing particles
	ParticleContainer::iterator writer = mParticles.begin( );
	for( ParticleContainer::iterator reader = mParticles.begin( ); reader != mParticles.end( ); ++reader )
	{
		// Apply movement and decrease lifetime
		updateParticle( *reader, dt );

		// If current particle is not dead
		if( reader->mPassedLifetime < reader->mTotalLifetime )
		{
			// Only apply EffectModifiers to living particles
			for( auto& EffectModifierPair : mEffectModifiers )
			{
				EffectModifierPair.function( *reader, true, dt );
			}
			ParticleEvent movPtEvt;
			Particle movPt = *reader;
			movPtEvt.mEventType = ParticleEventType::Move;
			movPtEvt.mPosition = movPt.mPosition;
			movPtEvt.mTotalLifetime = movPt.mTotalLifetime;
			movPtEvt.mVelocity = movPt.mVelocity;
			movPtEvt.mParentEmitter = movPt.mParentEmitter;
			mParticleEventSys.triggerEvent( movPtEvt );
			// Go ahead
			*writer++ = *reader;
		}
		else
		{
			// Send event about particle that die in current frame
			ParticleEvent diePtEvt;
			Particle diePt = *reader;
			diePtEvt.mEventType = ParticleEventType::Die;
			diePtEvt.mPosition = diePt.mPosition;
			diePtEvt.mTotalLifetime = diePt.mTotalLifetime;
			diePtEvt.mVelocity = diePt.mVelocity;
			diePtEvt.mParentEmitter = diePt.mParentEmitter;
			mParticleEventSys.triggerEvent( diePtEvt );
		}
	}

	// Remove particles dying this frame
	mParticles.erase( writer, mParticles.end( ) );

	// Remove EffectModifierors expiring this frame
	for( EffectModifPredContainer::iterator itr = mEffectModifiers.begin( ); itr != mEffectModifiers.end( ); )
	{
		incrementCheckExpiry( mEffectModifiers, itr, dt );
	}

}

void ParticleSystem::clearParticles( )
{
	mParticles.clear( );
}

void ParticleSystem::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
	// Check cached rectangles
	if( mNeedsQuadUpdate )
	{
		computeQuads( );
		mNeedsQuadUpdate = false;
	}

	// Check cached vertices
	if( mNeedsVertexUpdate )
	{
		computeVertices( );
		mNeedsVertexUpdate = false;
	}

	// Draw the vertex array with our texture
	states.texture = mTexture;
	target.draw( mVertices, states );
}

void ParticleSystem::emitParticle( const Particle& particle )
{
	ParticleEvent newPt;
	newPt.mEventType = ParticleEventType::Born;
	newPt.mPosition = particle.mPosition;
	newPt.mTotalLifetime = particle.mTotalLifetime;
	newPt.mVelocity = particle.mVelocity;
	newPt.mParentEmitter = particle.mParentEmitter;
	mParticleEventSys.triggerEvent( newPt );
	
	mParticles.push_back( particle );
}

void ParticleSystem::updateParticle( Particle& particle, sf::Time dt )
{
	particle.mPassedLifetime += dt;

	particle.mPosition += dt.asSeconds( ) * particle.mVelocity;
	particle.mRotation += dt.asSeconds( ) * particle.mRotationSpeed;
}

void ParticleSystem::computeVertices( ) const
{
	// Clear vertex array (keeps memory allocated)
	mVertices.clear( );

	// Fill vertex array
	for( auto &p : mParticles )
	{
		sf::Transform transform;
		transform.translate( p.mPosition );
		transform.rotate( p.mRotation );
		transform.scale( p.mScale );

		// Ensure valid index -- if this fails, you have not called addTextureRect() enough times, or p.textureIndex is simply wrong
		assert( p.mTextureIndex == 0 || p.mTextureIndex < mTextureRects.size( ) );

		const auto& quad = mQuads[p.mTextureIndex];
		for( std::size_t i = 0; i < 4; ++i )
		{
			sf::Vertex vertex;
			vertex.position = transform.transformPoint( quad[i].position );
			vertex.texCoords = quad[i].texCoords;
			vertex.color = p.mColor;

			mVertices.append( vertex );
		}
	}
}

void ParticleSystem::computeQuads( ) const
{
	// Ensure setTexture() has been called
	assert( mTexture );

	// No texture rects: Use full texture, cache single rectangle
	if( mTextureRects.empty( ) )
	{
		mQuads.resize( 1 );
		computeQuad( mQuads[0], fullRect( *mTexture ) );
	}

	// Specified texture rects: Cache every one
	else
	{
		mQuads.resize( mTextureRects.size( ) );
		for( std::size_t i = 0; i < mTextureRects.size( ); ++i )
			computeQuad( mQuads[i], mTextureRects[i] );
	}
}

void ParticleSystem::computeQuad( Quad& quad, const sf::IntRect& textureRect ) const
{
	sf::FloatRect rect( textureRect );

	quad[0].texCoords = sf::Vector2f( rect.left, rect.top );
	quad[1].texCoords = sf::Vector2f( rect.left + rect.width, rect.top );
	quad[2].texCoords = sf::Vector2f( rect.left + rect.width, rect.top + rect.height );
	quad[3].texCoords = sf::Vector2f( rect.left, rect.top + rect.height );

	quad[0].position = sf::Vector2f( -rect.width, -rect.height ) / 2.f;
	quad[1].position = sf::Vector2f( rect.width, -rect.height ) / 2.f;
	quad[2].position = sf::Vector2f( rect.width, rect.height ) / 2.f;
	quad[3].position = sf::Vector2f( -rect.width, rect.height ) / 2.f;
}

Connection ParticleSystem::addParticleEventListener( ParticleEventType eventType,
	std::function<void( const ParticleEvent& )> listener )
{
	return mParticleEventSys.connect( eventType, listener );
}

ParticleEventType eventId( const ParticleEvent& evt )
{
	return evt.mEventType;
}


