#include "Common.hxx"
#include "ConfigFile.hxx"
#include <iostream>
#include <fstream>

ConfigFile::ConfigFile( )
{

}

ConfigFile::~ConfigFile( )
{
	reset( );
}

void ConfigFile::load( const sf::String& fileName )
{
	std::ifstream cfgFileStream( fileName.toAnsiString( ).c_str( ) , std::ios_base::in );
	std::string str_line;
	while( std::getline( cfgFileStream, str_line ) )
	{
		processLine( str_line );
	}
}

void ConfigFile::reset( )
{
	mCfgMap.clear( );
}

int	ConfigFile::intValueOf( const sf::String& varName, int defVal ) const
{
	static sf::String value;
	value.clear( );
	
	auto ret = findString( varName, value );
	if( ret )
	{
		return std::stoi( value.toAnsiString( ) );
	}
	else
	{
		return defVal;
	}
}

float ConfigFile::floatValueOf( const sf::String& varName, float defVal ) const
{
	static sf::String value;
	value.clear( );
	auto ret = findString( varName, value );
	if( ret )
	{
		return std::stof( value.toAnsiString( ) );
	}
	else
	{
		return defVal;
	}
}

bool ConfigFile::boolValueOf( const sf::String& varName, bool defVal ) const
{
	const std::string TRUE_STR = "TRUE";
	const std::string ZERO_STR = "0";

	static sf::String value;
	value.clear( );
	auto ret = findString( varName, value );
	if( ret )
	{
		auto cvalue = value.toAnsiString();
		for (auto & c: cvalue ) c = toupper(c);
		//std::cout << cvalue;
		if( cvalue ==  TRUE_STR || 
		    cvalue == ZERO_STR )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return defVal;
	}
}

sf::String ConfigFile::stringValueOf( const sf::String& varName ) const
{
	static sf::String dummyStr;
	static sf::String value;
	value.clear( );
	auto ret = findString( varName, value );
	if( ret )
	{
		return value;
	}
	else
	{
		return dummyStr;
	}
}

sf::String ConfigFile::cfgFileName( ) const
{
	return mCfgFileName;
}

void ConfigFile::processLine( std::string& str )
{
	if( str.length( ) == 0 )
	{
		return;
	}

	str.erase( std::remove_if( str.begin( ), str.end( ), isspace ), str.end( ) );
	if( str[0] == '#' )//It comment line, skip
	{
		return;
	}
	auto namepos = str.find_first_of( "=", 0 );
	std::string name = str.substr( 0, namepos );
	namepos = str.find_first_not_of( "=", namepos );
	std::string value = str.substr( namepos );
	std::pair<sf::String,sf::String> newItem = std::make_pair<sf::String,sf::String>( name, value );
	mCfgMap.insert( newItem );
}

bool ConfigFile::findString( const sf::String& name, sf::String& value ) const 
{
	auto item = mCfgMap.find( name );
	if( item != mCfgMap.end( ) )
	{
		value = item->second;
		return true;
	}
	else
	{
		return false;
	}
}