#include "Common.hxx"
#include "Application.hxx"
#include "AppPrivate.hxx"
#include "IPlayerController.hxx"
#include <SFML/OpenGL.hpp>

AppPrivate::AppPrivate( const sf::String& strCfgFileName, IPlayerController* pPC )
{
	mpPC = pPC;
	mpCurrentScene = nullptr;
	mCfgFile.load( strCfgFileName );
}


AppPrivate::~AppPrivate( )
{

}

void AppPrivate::addScene( IRenderScene* newScene, const sf::String& strSceneName, bool bSetAsCurrent /*= false*/ )
{
	assert( newScene );
	for( auto i : mRenderScenesList )
	{
		if( i.mName == strSceneName )
		{
			return;
		}
	}
	SceneItem newItem;
	newItem.mName = strSceneName;
	newItem.mpScene = newScene;
	mRenderScenesList.push_back(  newItem );

	if( bSetAsCurrent )
	{
		mpCurrentScene = newScene;
	}
}

const sf::String WND_WIDTH = "WndWidth";
const sf::String WND_HEIGHT = "WndHeight";
const sf::String WND_CAPTION = "WndCaption";
const sf::String WND_FULLSCR = "FullScreen";

int AppPrivate::run( )
{
	sf::VideoMode vm( mCfgFile.intValueOf( WND_WIDTH, 800 ),
		mCfgFile.intValueOf( WND_HEIGHT, 600 ) );
	
	sf::ContextSettings ctx;
	ctx.depthBits = 24;
	
	sf::Uint32 wndStyle;
	if( mCfgFile.boolValueOf( WND_FULLSCR, false ) )
	{
		wndStyle = sf::Style::Fullscreen;
	}
	else
	{
		wndStyle = sf::Style::Default;
	}

	mRenderWnd.create( vm, mCfgFile.stringValueOf( WND_CAPTION ), wndStyle, ctx  );

	mRenderWnd.setActive( );

	for( auto sceneItem : mRenderScenesList )
	{
		sceneItem.mpScene->onLoading( );
		sceneItem.mpScene->onResize( mRenderWnd.getSize( ).x, mRenderWnd.getSize( ).y );
	}
	
	if( !mpCurrentScene && mRenderScenesList.size( ) > 0 )
	{
		auto s = mRenderScenesList.begin( );
		SceneItem firstScene = *s;
		mpCurrentScene = firstScene.mpScene;
	}

	if( mpCurrentScene )
	{
		mpCurrentScene->onSetCurrent( );
	}

	mClock.restart( );
	// Start the game loop
	while( mRenderWnd.isOpen( ) )
	{
		// Process events
		mRenderWnd.setActive( );
		sf::Event event;
		while( mRenderWnd.pollEvent( event ) )
		{
			// Close window: exit
			if( event.type == sf::Event::Closed )
			{
				for( auto sceneItem : mRenderScenesList )
				{
					sceneItem.mpScene->onUnloading( );
				}
				quit( );
			}

			// On key event
			if( event.type == sf::Event::KeyPressed )
			{
				assert( mpPC );
				mpPC->onKeypressed( event.key.code );
			}

			// Resize event: adjust the viewport
			if( event.type == sf::Event::Resized )
			{
				auto v = mRenderWnd.getView( );
				v.setSize( (float)event.size.width, (float)event.size.height );
				mRenderWnd.setView( v );
				for( auto sceneItem : mRenderScenesList )
				{
					sceneItem.mpScene->onResize( event.size.width, event.size.height );
				}
			}
		}
		mRenderWnd.clear( ); 

		drawScene( );

		mRenderWnd.display( );
	}
	return 0;
}

void AppPrivate::drawScene( )
{
	sf::Time dT = mClock.restart( );
	if( !mpCurrentScene )
	{
		return;
	}
	mpCurrentScene->prepareFrame( dT );
	mpCurrentScene->renderFrame( mRenderWnd );
	mpCurrentScene->endFrame( );

	mClock.restart( );

}

void AppPrivate::quit( )
{
	mRenderWnd.close( );

	for( auto sceneItem : mRenderScenesList )
	{
		sceneItem.mpScene->onUnloading( );
	}
}