#include "Common.hxx"
#include "Particle.hxx"
#include "EffectModifiers.hxx"


ForceEffect::ForceEffect( sf::Vector2f acceleration )
	: mAcceleration( acceleration )
{
}

void ForceEffect::operator() ( Particle& particle, bool singleshot, sf::Time dt )
{
	particle.mVelocity += dt.asSeconds( ) * mAcceleration;
}

// ---------------------------------------------------------------------------------------------------------------------------


TorqueEffect::TorqueEffect( float angularAcceleration )
	: mAngularAcceleration( angularAcceleration )
{
}

void TorqueEffect::operator() ( Particle& particle, bool singleshot, sf::Time dt )
{
	particle.mRotationSpeed += dt.asSeconds( ) * mAngularAcceleration;
}

// ---------------------------------------------------------------------------------------------------------------------------


ScaleEffect::ScaleEffect( sf::Vector2f scaleFactor )
	: mScaleFactor( scaleFactor )
{
}

void ScaleEffect::operator() ( Particle& particle, bool singleshot, sf::Time dt )
{
	particle.mScale += dt.asSeconds( ) * mScaleFactor;
}

// ---------------------------------------------------------------------------------------------------------------------------

