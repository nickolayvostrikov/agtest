#pragma once
#include "ConfigFile.hxx"
#include "IRenderScene.hxx"

static Application* gpAppInstance = nullptr; 

struct SceneItem final
{
	IRenderScene* mpScene;
	sf::String mName;

	SceneItem( ) noexcept
	{
		mpScene = nullptr;
	}

	SceneItem( IRenderScene* pS, sf::String& SceneName ) noexcept
	{
		mpScene = pS;
		mName = SceneName;
	}

	SceneItem( const SceneItem& other ) noexcept
	{
		if( *this == other )
		{
			return;
		}
		mpScene = other.mpScene;
		mName = other.mName;
	}

	SceneItem( SceneItem&& other ) noexcept : mName( std::move( other.mName ) ),
		mpScene( std::move( other.mpScene ) )
	{
	}

	SceneItem& operator = ( const SceneItem& other ) noexcept
	{
		if( *this == other )
		{
			return *this;
		}
		mpScene = other.mpScene;
		mName = other.mName;
		return *this;
	}

	SceneItem& operator = ( const SceneItem&& other ) noexcept 
	{ 
		mName = std::move( other.mName );
		mpScene = std::move( other.mpScene );
		return *this;
	}

	bool operator == ( const SceneItem& other )
	{
		if( mName == other.mName && mpScene == other.mpScene )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};

typedef std::list<SceneItem> IRenderSceneList;

class AppPrivate final : private sf::NonCopyable
{
public:
	AppPrivate( const sf::String& strCfgFileName, IPlayerController* pPC );
	~AppPrivate( );

	IPlayerController* mpPC;
	ConfigFile mCfgFile;
	sf::RenderWindow mRenderWnd;
	sf::Clock mClock;

	IRenderSceneList mRenderScenesList;
	IRenderScene* mpCurrentScene;

	void addScene( IRenderScene* newScene, const sf::String& strSceneName, bool bSetAsCurrent = false );
	
	int run( );

	void drawScene( );

	void quit( );
};