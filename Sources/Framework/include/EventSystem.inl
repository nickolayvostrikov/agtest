#include "Common.hxx"
#include "EventListener.hxx"
#include "EventSystem.hxx"

template <typename Event, typename EventId>
EventSystem<Event, EventId>::EventSystem( )
	: mListeners( )
{
}

template <typename Event, typename EventId>
void EventSystem<Event, EventId>::triggerEvent( const Event& event )
{
	// Import symbol getEventId to qualify for ADL.
	mListeners.call( eventId( event ), event );
}

template <typename Event, typename EventId>
Connection EventSystem<Event, EventId>::connect( const EventId& trigger, std::function<void( const Event& )> unaryListener )
{
	return mListeners.add( trigger, std::move( unaryListener ) );
}

template <typename Event, typename EventId>
Connection EventSystem<Event, EventId>::connect0( const EventId& trigger, std::function<void( )> nullaryListener )
{
	return connect( trigger, std::bind( std::move( nullaryListener ) ) );
}

template <typename Event, typename EventId>
void EventSystem<Event, EventId>::clearConnections( EventId identifier )
{
	mListeners.clear( identifier );
}

template <typename Event, typename EventId>
void EventSystem<Event, EventId>::clearAllConnections( )
{
	mListeners.clearAll( );
}

template <typename Event, typename EventId>
EventSystem<Event, EventId>::EventSystem( const EventSystem<Event, EventId>&& right ) noexcept
{
	mListeners = std::move( right.mListeners );
}

template <typename Event, typename EventId>
EventSystem<Event, EventId>& EventSystem<Event, EventId>::operator = ( const EventSystem<Event, EventId>&& right ) noexcept
{
	mListeners = std::move( right.mListeners );
	return *this;
}

// ---------------------------------------------------------------------------------------------------------------------------


namespace detail
{

	// Default implementation for events where the Event type is the same as the EventId type.
	template <typename Event>
	const Event& eventId( const Event& event )
	{
		return event;
	}

} // namespace detail
