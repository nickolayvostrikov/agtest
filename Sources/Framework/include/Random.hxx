#pragma once

//Returns an int random number in the interval [min, max].
int random( int min, int max );

//Returns an unsigned int random number in the interval [min, max].
unsigned int random( unsigned int min, unsigned int max );

//Returns a float random number in the interval [min, max].
float random( float min, float max );

//Returns a float random number in the interval [middle-deviation, middle+deviation].
float randomDev( float middle, float deviation );

//Sets the seed of the random number generator.
void setRandomSeed( unsigned long seed );
