#pragma once

#include <type_traits>
#include <utility>

namespace Utils
{

	/*
	*  Just std::make_unique from CXX 14 standart 
	*/
	template <typename T, typename... Args>
	std::unique_ptr<T> make_unique_helper( std::false_type, Args&&... args )
	{
		return std::unique_ptr<T>( new T( std::forward<Args>( args )... ) );
	}

	template <typename T, typename... Args>
	std::unique_ptr<T> make_unique_helper( std::true_type, Args&&... args )
	{
		static_assert( std::extent<T>::value == 0,
			"make_unique<T[N]>() is forbidden, please use make_unique<T[]>()." );

		typedef typename std::remove_extent<T>::type U;
		return std::unique_ptr<T>( new U[sizeof...( Args )]{std::forward<Args>( args )...} );
	}

	template <typename T, typename... Args>
	std::unique_ptr<T> make_unique( Args&&... args )
	{
		return make_unique_helper<T>( std::is_array<T>( ), std::forward<Args>( args )... );
	}
	

	// Functor that returns always the same value (don't use lambda expression )
	template <typename T>
	struct Constant
	{
		explicit Constant( T value )
			: value( value )
		{
		}

		T operator() ( ) const
		{
			return value;
		}

		T value;
	};

	//Function to calc percents of value T
	template<typename T> T percentsOf( T value, T percetns )
	{
		return ( value / T(100) ) * percetns;
	}

	template<typename T> T pi( ) 
	{ 
		return T(3.141592653589793238462643383f); 
	}
	template<typename T> T radToDeg( T rad ) 
	{ 
		return T(180) / pi<T>( ) * rad; 
	}
	template<typename T> T degToRad( T deg ) 
	{ 
		return pi<T>( ) / T(180) * deg; 
	}

	void initWorkingDirFromArv0( const char* argv0 );

	std::string workingDir( );
}