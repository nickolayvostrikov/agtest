#pragma once

class IRenderScene : private sf::NonCopyable 
{
protected:
	unsigned int mWidth;
	unsigned int mHeight;
public:

	IRenderScene( ): mWidth( 0 ), mHeight( 0 )
	{

	}

	virtual ~IRenderScene( )
	{

	}

	virtual void onLoading( ) = 0;

	virtual void onUnloading( ) = 0;
 
	virtual void onResize( unsigned int newWidth, unsigned int newHeight )
	{
		mWidth = newWidth;
		mHeight = newHeight;
	}

	virtual void onSetCurrent( ) = 0;

	virtual void onLoseCurrent( ) = 0;

	virtual void prepareFrame( sf::Time deltaTime ) = 0;

	virtual void renderFrame( sf::RenderWindow& rt ) = 0;

	virtual void endFrame( ) = 0;
};