#pragma once
#include "Utils.hxx"

// Vector in polar coordinate system
template <typename T>
struct PolarVector2
{
	T	r;				// Radius
	T	phi;			//Angle in degrees

						
	PolarVector2( );

	//Constructs a polar vector with specified radius and angle.
	PolarVector2( T radius, T angle );

	// @brief Constructs a polar vector from a cartesian SFML vector.
	PolarVector2( const sf::Vector2<T>& vector );

	//Converts the polar vector into a cartesian SFML vector.
	operator sf::Vector2<T>( ) const;
};

//Type definition for float polar vectors
typedef PolarVector2<float> PolarVector2f;


template <typename T>
T length( const PolarVector2<T>& vector );


template <typename T>
T polarAngle( const PolarVector2<T>& vector );


template <typename T>
PolarVector2<T>::PolarVector2( )
	: r( )
	, phi( )
{
}

template <typename T>
PolarVector2<T>::PolarVector2( T radius, T angle )
	: r( radius )
	, phi( angle )
{
}

template <typename T>
PolarVector2<T>::PolarVector2( const sf::Vector2<T>& vector )
	: r( length( vector ) )
	, phi( vector == sf::Vector2<T>( ) ? 0.f : polarAngle( vector ) )
{
}

template <typename T>
PolarVector2<T>::operator sf::Vector2<T>( ) const
{
	return sf::Vector2<T>(
		r * cos( Utils::degToRad( phi ) ),
		r * sin( Utils::degToRad( phi ) ) );
}

template <typename T>
T length( const PolarVector2<T>& vector )
{
	return vector.r;
}

template <typename T>
T polarAngle( const PolarVector2<T>& vector )
{
	return vector.phi;
}
