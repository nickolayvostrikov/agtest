#pragma once

#include "EventListener.hxx"
#include "Connection.hxx"


template <typename Event, typename EventId = Event>
class EventSystem: private sf::NonCopyable
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:

	EventSystem( );

	EventSystem( const EventSystem<Event, EventId>&& right ) noexcept;

	EventSystem<Event, EventId>& operator = ( const EventSystem<Event, EventId>&& right ) noexcept;
	
	void						triggerEvent( const Event& event );

	Connection					connect( const EventId& trigger, std::function<void( const Event& )> unaryListener );
	
	Connection					connect0( const EventId& trigger, std::function<void( )> nullaryListener );
	
	void						clearConnections( EventId identifier );
	
	void						clearAllConnections( );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private types
private:
	// Container type
	typedef ListenerMap<EventId, const Event&>	EventListenerMap;


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	EventListenerMap			mListeners;
};

#include "EventSystem.inl"

