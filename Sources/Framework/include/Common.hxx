#pragma once

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <cassert>
#include <cmath>


#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <array>
#include <utility>
#include <string>
#include <algorithm>
#include <functional>
#include <memory>

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Types.hxx"
#include "Defines.hxx"
#include "Utils.hxx"
