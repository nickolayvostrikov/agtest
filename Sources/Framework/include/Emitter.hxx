#pragma once

class Particle;


constexpr int32_t INVALID_EMITTER_ID = ( - 1 );

class EmissionInterface
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	
	virtual						~EmissionInterface( ) {}

	virtual void				emitParticle( const Particle& particle ) = 0;
};

template <typename Emitter>
std::function<void( EmissionInterface&, bool, sf::Time )> refEmitter( Emitter& referenced )
{
	return [&referenced]( EmissionInterface& system, bool bSingleshot, sf::Time dt )
	{
		return referenced( system, dt );
	};
}


class BaseEmitter
{
protected:
	int32_t                     miEmitterID;
	Distribution<unsigned int>	mParticleTextureIndex;
public:
	BaseEmitter(  int32_t id, unsigned int texId ) :
		 miEmitterID( id )
		,mParticleTextureIndex( texId )
	{
	}
	
	virtual ~BaseEmitter( )
	{

	}

	virtual void   operator() ( EmissionInterface& system, sf::Time dt )  = 0;

	int32_t emitterID( ) const 
	{ 
		return miEmitterID; 
	};

	void setEmitterID( int32_t newEmitID )
	{
		miEmitterID = newEmitID;
	}

	void setParticleTextureIndex( Distribution<unsigned int> texIndex )
	{
		mParticleTextureIndex = std::move( texIndex );
	}
};





