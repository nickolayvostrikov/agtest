#pragma once

#define F_UNUSED( x ) (void)x

#define F_INVALID_INDEX ( -1 )


#define FSAFE_DELETE( p ) if( p ) { delete p; p = nullptr; }
#define FSAFE_DELETE_ARRAY( p ) if( p ) { delete[] p; p = nullptr; }

#define VARIADIC_ENABLE_IF(...)  , typename std::enable_if<__VA_ARGS__>::type* = nullptr
