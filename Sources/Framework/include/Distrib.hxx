#pragma once


#include "Utils.hxx"

template <typename T>
class Distribution;

// Metafunction for SFINAE and reasonable compiler errors
template <typename Fn, typename T>
struct IsCompatibleFunction
{
	// General case: Fn is a functor/function -> if it's not convertible to T (and thus not a constant), accept it
	static const bool value = !std::is_convertible<Fn, T>::value;
};

template <typename U, typename T>
struct IsCompatibleFunction<Distribution<U>, T>
{
	// If Fn is another Distribution<U>, accept it if U is convertible to T (like all functors, but clearer error message)
	static const bool value = std::is_convertible<U, T>::value;
};

template <typename T>
class Distribution
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Private types
private:
	typedef std::function<T( )> FactoryFn;


	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	
	template <typename U>
	Distribution( U constant
		VARIADIC_ENABLE_IF( std::is_convertible<U, T>::value ) )
		: mFactory( Utils::Constant<T>( constant ) )
	{
	}

	
	template <typename Fn>
	Distribution( Fn function
		VARIADIC_ENABLE_IF( IsCompatibleFunction<Fn, T>::value ) )
		: mFactory( function )
	{
	}


	
	T	operator() ( ) const
	{
		return mFactory( );
	}


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	FactoryFn					mFactory;
};


Distribution<int> uniform( int min, int max );


Distribution<unsigned int> uniform( unsigned int min, unsigned int max );


Distribution<float> uniform( float min, float max );


Distribution<sf::Time> uniform( sf::Time min, sf::Time max );


Distribution<sf::Vector2f> rect( sf::Vector2f center, sf::Vector2f halfSize );


Distribution<sf::Vector2f> circle( sf::Vector2f center, float radius );


Distribution<sf::Vector2f> deflect( sf::Vector2f direction, float maxRotation );

