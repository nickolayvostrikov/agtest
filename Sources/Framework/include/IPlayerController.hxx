#pragma  once

/*
This very powerful class filled with spirit of Unreal Engine 4!
UE4 is best graphics engine and editor on the world!
:)))
*/
class IPlayerController : private sf::NonCopyable
{
public:
	IPlayerController( )
	{

	}
	virtual ~IPlayerController( )
	{

	}

	virtual void onKeypressed( sf::Keyboard::Key& k ) = 0;
};