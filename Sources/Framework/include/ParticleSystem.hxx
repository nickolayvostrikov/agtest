#pragma once
#include "EventSystem.hxx"

class AbstractConnectionImpl;

enum class ParticleEventType
{
	Born,
	Move,
	Die
};

struct ParticleEvent final
{
	ParticleEventType    mEventType;
	BaseEmitter*         mParentEmitter;
	sf::Vector2f	     mPosition;
	sf::Vector2f		 mVelocity;
	sf::Time			 mTotalLifetime;
};


class ParticleSystem : public sf::Drawable, private sf::NonCopyable, private EmissionInterface
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Private types
private:
	// Type to store EffectModifieror or emitter + time until removal + id for removal
	template <typename Signature>
	struct Function
	{
		Function( std::function<Signature> function, bool singleshot,sf::Time timeUntilRemoval )
			: function( std::move( function ) )
			, singleshot( singleshot )
			, timeUntilRemoval( timeUntilRemoval )
			, id( nextId( ) )
			, tracker( )
			, beforeshot( true )
		{
		}

		static unsigned int nextId( )
		{
			static unsigned int next = 0;
			return next++;
		}

		std::function<Signature>						function;
		bool                                            singleshot;
		bool                                            beforeshot;
		sf::Time										timeUntilRemoval;
		unsigned int									id;
		std::shared_ptr<AbstractConnectionImpl> tracker;
	};

	// Vertex quads, used to cache texture rectangles
	typedef std::array<sf::Vertex, 4>					Quad;

	// Function typedefs
	typedef Function<void( Particle&, bool, sf::Time )>			EffectModifPred;
	typedef Function<void( EmissionInterface&, bool, sf::Time )>	Emitter;

	// Container typedefs
	typedef std::vector<Particle>						ParticleContainer;
	typedef std::vector<EffectModifPred>				EffectModifPredContainer;
	typedef std::vector<Emitter>						EmitterContainer;

	typedef EventSystem<ParticleEvent, ParticleEventType> ParticleEventSystem;
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	
	ParticleSystem( );

	ParticleSystem( const ParticleSystem&& source ) noexcept;

	
	ParticleSystem&	operator= ( const ParticleSystem&& source ) noexcept;

	
	void         setTexture( const sf::Texture& texture );

	
	unsigned int addTextureRect( const sf::IntRect& textureRect );

	
	Connection   addEffectModifier( std::function<void( Particle&, bool, sf::Time )> effectMod );

	
	Connection   addEffectModifier( std::function<void( Particle&, bool, sf::Time )> effectMod, sf::Time timeUntilRemoval );

	
	void         clearEffectModifiers( );

	
	Connection	 addEmitter( std::function<void( EmissionInterface&, bool, sf::Time )> emitter );

	Connection	 addEmitter( std::function<void( EmissionInterface&, bool, sf::Time )> emitter, bool singleshot );

	Connection	 addEmitter( std::function<void( EmissionInterface&, bool, sf::Time )> emitter, bool singleshot, sf::Time timeUntilRemoval );

	
	void		 clearEmitters( );


	void		 update( sf::Time dt );

	
	void	     clearParticles( );

	Connection  addParticleEventListener( ParticleEventType eventType, std::function<void( const ParticleEvent& )> listener );

	// ---------------------------------------------------------------------------------------------------------------------------
	// Private member functions
private:
	
	virtual void				draw( sf::RenderTarget& target, sf::RenderStates states ) const;

	
	virtual void				emitParticle( const Particle& particle );

	// Updates a single particle.
	void						updateParticle( Particle& particle, sf::Time dt );

	// Recomputes the vertex array.
	void						computeVertices( ) const;

	// Recomputes the cached rectangles (position and texCoords quads)
	void						computeQuads( ) const;
	void						computeQuad( Quad& quad, const sf::IntRect& textureRect ) const;


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	ParticleContainer			mParticles;
	EffectModifPredContainer	mEffectModifiers;
	EmitterContainer			mEmitters;

	const sf::Texture*			mTexture;
	std::vector<sf::IntRect>	mTextureRects;

	mutable sf::VertexArray		mVertices;
	mutable bool				mNeedsVertexUpdate;
	mutable std::vector<Quad>	mQuads;
	mutable bool				mNeedsQuadUpdate;
	ParticleEventSystem         mParticleEventSys;
};


ParticleEventType eventId( const ParticleEvent& evt );






