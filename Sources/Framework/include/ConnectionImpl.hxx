#pragma once

// Abstract class that allows to disconnect listeners using type erasure
class AbstractConnectionImpl
{
public:
	// Disconnects a listener from an event
	virtual void disconnect( ) = 0;

	// Virtual destructor
	virtual ~AbstractConnectionImpl( )
	{
	}
};

// Concrete class implementing the actual disconnection for std::list and iterator
template <typename List>
class IteratorConnectionImpl : public AbstractConnectionImpl
{
private:
	typedef typename List::Iterator Iterator;

public:
	// Constructor
	IteratorConnectionImpl( List& container, Iterator iterator )
		: mContainer( &container )
		, mIterator( iterator )
	{
	}

	virtual void disconnect( )
	{
		mContainer->remove( mIterator );
	}

private:
	List*		mContainer;
	Iterator	mIterator;
};

// Concrete class implementing the actual disconnection for any container and ID
template <typename Container>
class IdConnectionImpl : public AbstractConnectionImpl
{
private:
	typedef typename Container::value_type ValueType;

public:
	// Constructor
	IdConnectionImpl( Container& container, unsigned int id )
		: mContainer( &container )
		, mId( id )
	{
	}

	virtual void disconnect( )
	{
		//Maybe use binary search
		auto found = std::find_if( mContainer->begin( ), mContainer->end( ), [this]( ValueType& v ) { return v.id == mId; } );

		if( found != mContainer->end( ) )
			mContainer->erase( found );
	}

private:
	Container*			mContainer;
	unsigned int		mId;
};

template <typename List>
std::shared_ptr<IteratorConnectionImpl<List>> makeIteratorConnectionImpl( List& container, typename List::Iterator iterator )
{
	return std::make_shared<IteratorConnectionImpl<List>>( container, iterator );
}

template <typename Container>
std::shared_ptr<IdConnectionImpl<Container>> makeIdConnectionImpl( Container& container, unsigned int id )
{
	return std::make_shared<IdConnectionImpl<Container>>( container, id );
}
