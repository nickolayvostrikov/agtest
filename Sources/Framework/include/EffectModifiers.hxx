#pragma once

class Particle;


template <typename EffectModifier>
std::function<void( Particle&, sf::Time )> refEffectModifier( EffectModifier& referenced )
{
	return [&referenced]( Particle& particle, sf::Time dt )
	{
		return referenced( particle, dt );
	};
}



//EffectModifier class that applies an acceleration vector to each particle. A popular use case is gravity.
class ForceEffect
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	explicit					ForceEffect( sf::Vector2f acceleration );

	void						operator() ( Particle& particle, bool singleshot, sf::Time dt );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	sf::Vector2f				mAcceleration;
};

//Applies a rotational acceleration to particles over time.
class TorqueEffect
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	explicit					TorqueEffect( float angularAcceleration );

	void						operator() ( Particle& particle, bool singleshot, sf::Time dt );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	float						mAngularAcceleration;
};

//Scales particles over time.
class ScaleEffect
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	explicit					ScaleEffect( sf::Vector2f scaleFactor );

	void						operator() ( Particle& particle, bool singleshot, sf::Time dt );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	sf::Vector2f				mScaleFactor;
};



