#pragma once

#include "EventSystem.hxx" 

class BaseEmitter;

class Particle final
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:

	explicit					Particle( sf::Time timeTotalLife );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Public variables
public:
	sf::Vector2f				                   mPosition;  // Current position.
	sf::Vector2f				                   mVelocity;  // Velocity (change in position per second).
	float						                   mRotation;  // Current rotation angle.
	float						                   mRotationSpeed;  // Angular velocity (change in rotation per second).
	sf::Vector2f				                   mScale;  // Scale, where (1,1) represents the original size.
	sf::Color					                   mColor;  // %Particle color.
	unsigned int				                   mTextureIndex;  // Index of the used texture rect, returned by ParticleSystem::addTextureRect()
	BaseEmitter*                                   mParentEmitter; //Pointer to emitter
    // ---------------------------------------------------------------------------------------------------------------------------
    // Private variables
private:
	sf::Time					mPassedLifetime;    // Time passed since emitted.
	sf::Time					mTotalLifetime;		// Total time to live.


	// ---------------------------------------------------------------------------------------------------------------------------
    // Friends
													
	
	friend class ParticleSystem;
	friend sf::Time elapsedLifetime( const Particle& particle );
	friend sf::Time  totalLifetime( const Particle& particle );
	friend void  abandonParticle( Particle& particle );
};


sf::Time elapsedLifetime( const Particle& particle );


sf::Time totalLifetime( const Particle& particle );


sf::Time remainingLifetime( const Particle& particle );


float elapsedRatio( const Particle& particle );


float remainingRatio( const Particle& particle );


void abandonParticle( Particle& particle );

