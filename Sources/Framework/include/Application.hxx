#pragma once

class AppPrivate;
class IRenderScene;
class IPlayerController;
class ConfigFile;

class Application final : private sf::NonCopyable
{
private:
	std::unique_ptr<AppPrivate> mpImpl;
public:

	Application( const sf::String& strConfigFileName, IPlayerController* pPlayCtrl );
	~Application( );

	void addScene( IRenderScene* newScene, const sf::String& strSceneName, bool bSetAsCurrent = false );

	int run(  );

	void quit( );

	void switchSceneTo( const sf::String& strNewSceneName );

	static Application* instance( );

	const ConfigFile* configFile( );
};

inline Application* FApp( )
{
	return Application::instance( );
}
