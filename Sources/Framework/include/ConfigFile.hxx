#pragma once

#include <map>

typedef std::map<sf::String,sf::String> ConfFileMap;


class ConfigFile final : private sf::NonCopyable
{
private:
	ConfFileMap mCfgMap;
	sf::String  mCfgFileName;

	void processLine( std::string& str );
	bool findString( const sf::String& name, sf::String& value ) const;
public:
	ConfigFile( );
	~ConfigFile();

	void load( const sf::String& fileName );

	void reset( );

	int	intValueOf( const sf::String& varName, int defVal = 0 ) const;

	float floatValueOf( const sf::String& varName, float defVal = 0.0f ) const;

	bool boolValueOf( const sf::String& varName, bool defVal = false ) const;

	sf::String  stringValueOf( const sf::String& varName ) const;

	sf::String cfgFileName( ) const;
};