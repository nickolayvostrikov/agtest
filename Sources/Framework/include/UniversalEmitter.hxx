#pragma once

#include "Emitter.hxx"

class UniversalEmitter : public BaseEmitter
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	UniversalEmitter( int32_t id, unsigned int texId );

	virtual ~UniversalEmitter( );
	
	virtual void operator() ( EmissionInterface& system, sf::Time dt ) override;

	void setEmissionRate( float particlesPerSecond );

	void setParticleLifetime( Distribution<float> prtLifetimeMin, Distribution<float> prtLifetimeMax );

	void setParticlePosition( Distribution<sf::Vector2f> particlePosition );

	void setParticleVelocity( Distribution<sf::Vector2f> particleVelocity );

	void setParticleVelocityModifier( Distribution<float> particleVelMod );

	void setParticleRotation( Distribution<float> particleRotation );

	void setParticleRotationSpeed( Distribution<float> particleRotationSpeed );

	void setParticleScale( Distribution<sf::Vector2f> particleScale );

	void setParticleColor( Distribution<sf::Color> particleColor );

	// ---------------------------------------------------------------------------------------------------------------------------
	// Private member functions
private:
	// Returns the number of particles to emit during this frame.
	std::size_t					computeParticleCount( sf::Time dt );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	float						mEmissionRate;
	float						mEmissionDifference;

	Distribution<float>		    mParticleLifeTimeMin;
	Distribution<float>		    mParticleLifeTimeMax;
	Distribution<sf::Vector2f>	mParticlePosition;
	Distribution<sf::Vector2f>	mParticleVelocity;
	Distribution<float>         mParticleVelocityModifier;
	Distribution<float>			mParticleRotation;
	Distribution<float>			mParticleRotationSpeed;
	Distribution<sf::Vector2f>	mParticleScale;
	Distribution<sf::Color>		mParticleColor;
};




