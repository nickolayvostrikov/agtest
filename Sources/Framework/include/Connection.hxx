#pragma once

class AbstractConnectionImpl;
//  The class %Connection uses shared-ownership semantics, that means copying its instances results in multiple references to
//  the same listener. A connection automatically invalidates when the referenced object is removed. Therefore, you don't have
//  to worry about dangling connections.
class Connection
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	
	Connection( );

	
	bool						isConnected( ) const;

	
	void						invalidate( );

	
	void						disconnect( );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Implementation details
public:
	// Create connection from tracker object
	explicit					Connection( std::weak_ptr<AbstractConnectionImpl> tracker );


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	std::weak_ptr<AbstractConnectionImpl> mWeakRef;
};



class ScopedConnection : private sf::NonCopyable
{
	// ---------------------------------------------------------------------------------------------------------------------------
	// Public member functions
public:
	
	ScopedConnection( );

	
	explicit					ScopedConnection( const Connection& connection );

	
	ScopedConnection( ScopedConnection&& source );

	
	ScopedConnection&			operator= ( ScopedConnection&& source );

	
	~ScopedConnection( );

	
	bool						isConnected( ) const;


	// ---------------------------------------------------------------------------------------------------------------------------
	// Private variables
private:
	Connection					mConnection;
};
